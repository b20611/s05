import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        //System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe","+639152468596","Quezon City");
        Contact contact2 = new Contact("Jane Doe","+639162148573","Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);


        if(phonebook.getContacts().size() == 0) {
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : phonebook.getContacts()) {

//                System.out.println(contact.getName());
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getAddress());
            }
        }

    }
}