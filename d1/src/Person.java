public class Person implements Actions,Greetings{
    //for a class to implement or use an interface, we use the implements keyword.

    public void sleep(){
        System.out.println("Zzzzzzz....");
    }

    public void run(){
        System.out.println("Running on the road!");
    }

    public void morningGreet(){
        System.out.println("Good Morning, Friend!");
    }
    public void holidayGreet(){
        System.out.println("Happy Holidays, Friend!");
    }
}
