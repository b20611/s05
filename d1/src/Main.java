public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

    Person person1 = new Person();

    person1.run();
    person1.sleep();
    person1.morningGreet();
    person1.holidayGreet();

    Computer computer1 = new Computer();

    computer1.run();
    computer1.sleep();

    Parent parent1 = new Parent("Alvin",35);
    parent1.greet();
    parent1.greet("Arvin","morning");

    StaticPoly staticPoly = new StaticPoly();

        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,5,10));
        System.out.println(staticPoly.addition(15.5,15.6));

    parent1.introduce();
    Child newChild = new Child("Alvin Jr.", 3);
    newChild.introduce();
    }
}