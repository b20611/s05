public interface Actions {

    //Interfaces are used to achieve abstraction. It hides away the actual implementation of the methods and simply shows a "list"/ a "menu" of methods that can be done.
    //Interfaces are like blueprints for your clasess. Because any class that implements interfaces MUST have the methods in the interface.

    public void sleep();
    public void run();
}
