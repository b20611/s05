public class Parent {

    private String name;
    private int age;

    /*
        Polymorphism
            Static Polymorphism
            This is the ability to have multiple methods of the same name but changes forms based on the number of arguments or the tye of arguments
    */
    public Parent() {
    }

    public Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet(){
        System.out.println("Hell friend!");
    }
    //Overloading - polymorphism achieved by creating a method of the same name but with a different number of arguments
    public void greet(String name, String timeOfDay){
        System.out.println("Good " + timeOfDay + " !, " + name);
    }

    //Dynamic Method Dispatch or Runtime polymorphism, the ability of a subclass to inherit a method from a parent but override it and change its definition in the sub-class.
    public void introduce(){
        System.out.println("Hi! I'm " + this.name + ". I am a parent.");
    }
}
